#! /usr/bin/env python3

import os
import shutil
import subprocess
import sys
import tempfile

ROOT='/home/tfheen/debian/dsa-wiki'
BASE='docker.io/debian:testing'
REGISTRY='docker://docker-registry-priv.debian.org'
IMAGE='dsa-wiki'

def main():
    global ROOT, IMAGE
    if len(sys.argv) > 3:
        raise RuntimeError("bad cmdline")
    if len(sys.argv) >= 2:
        ROOT = os.path.abspath(sys.argv[1])
    if len(sys.argv) == 3:
        IMAGE = sys.argv[2]
    tempdir = tempfile.mkdtemp()
    assert ':' not in tempdir
    here = os.path.dirname(__file__)
    assert ':' not in here
    # maybe: podman pull docker.io/debian:testing
    subprocess.check_call(['podman', 'run', '-it',
                           '-v', '{}:/build'.format(ROOT),
                           '-v','%s:/dsa-build' % here,
                           '-v', '%s:/var/lib/containers' % tempdir,
                           '--device', '/dev/fuse',
                           '--env-file', 'build-env',
                           BASE,
                           "/bin/sh", "-c", "/dsa-build/prep;/build/deborg-build-container"])
    subprocess.check_call(['podman', '--root', os.path.join(tempdir, 'storage'), 'images', IMAGE, '--format', 'json'])
    subprocess.check_call(['podman', '--root', os.path.join(tempdir, 'storage'), 'push', IMAGE, REGISTRY])
#    shutil.rmtree(tempdir)

if __name__ == '__main__':
    main()
